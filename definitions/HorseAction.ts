export type HorseAction = {
    name: string;
    endpoint: string;
    shouldDeliver: (data: unknown) => boolean;
};
