import { HorseDestination } from './HorseDestination';

export type HorseConfiguration = {
    destinations: HorseDestination[];
    getOriginDestination?: (data: unknown) => string;
};
