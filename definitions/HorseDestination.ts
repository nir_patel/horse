import { HorseAction } from './HorseAction';

export type HorseDestination = {
    name: string;
    baseUrl: string;
    headers?: Record<string, string | number | boolean>;
    timeout: number;
    enabled: boolean;
    actions: HorseAction[];
    priority?: number;
};
