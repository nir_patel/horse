import { deliver } from './src/deliver';
import { init } from './src/init';

export { deliver } from './src/deliver';
export { init } from './src/init';
export { originDestinationFunction } from './src/getOriginDestinationFunction';
export { enable, disable } from './src/toggle';

const Horse = { init, deliver };

export default Horse;
