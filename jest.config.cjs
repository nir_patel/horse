module.exports = {
    verbose: true,
    bail: false,
    testMatch: ['**/tests/*.spec.(js|ts)'],
    testPathIgnorePatterns: ['<rootDir>/node_modules/'],
    preset: 'ts-jest',
    testEnvironment: 'node'
};
