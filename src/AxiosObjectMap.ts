import { AxiosInstance } from 'axios';

const axiosObjectMap: Record<string, AxiosInstance> = {};

export const add = (name: string, obj: AxiosInstance) => {
    axiosObjectMap[name] = obj;
};

export const remove = (name: string) => {
    delete axiosObjectMap[name];
};

export const getAll = () => axiosObjectMap;
