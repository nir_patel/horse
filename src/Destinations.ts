import { HorseDestination } from '../definitions/HorseDestination';

const destinations: HorseDestination[] = [];

export const add = (destination: HorseDestination) => {
    destinations.push(destination);
};

export const getAll = () => destinations;
