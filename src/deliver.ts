import { AxiosResponse } from 'axios';
import { getAll as getAllAxiosObjects } from './AxiosObjectMap';
import { getAll as getAllDestinations } from './Destinations';

/**
 *
 * Delivers your action data to the relevant destionations according to the configuration provided.
 * @param actionName the name of the action you want to inform your destinations about
 * @param data the data you want to send to your destinations about the action
 * @param callback optional: a callback to run after data is delivered
 */
export const deliver = (
    actionName: string,
    data: unknown,
    callback?: (res: AxiosResponse) => void
) => {
    const relevantDestionations = getAllDestinations().filter(dest => {
        const relevantAction = dest.actions.find(action => action.name === actionName);
        return dest.enabled && relevantAction?.shouldDeliver(data);
    });

    const maxPriority = Math.max(
        ...relevantDestionations.map(dest => dest.priority ?? 0)
    );

    const prioratizedOnlyDestinations = relevantDestionations.filter(
        dest => (dest.priority ?? 0) === maxPriority
    );

    prioratizedOnlyDestinations.forEach(dest => {
        const axiosObject = getAllAxiosObjects()[dest.name];
        axiosObject
            .post(dest.actions.find(action => action.name === actionName).endpoint, data)
            .then(callback);
    });
};
