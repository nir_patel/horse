type OriginDestinationFunction = ((data: unknown) => string) | undefined;

let getOriginDestination: OriginDestinationFunction;

/**
 *
 * @returns a function to map data to the name of the destinations
 */
export const originDestinationFunction = () => getOriginDestination;

export const set = (func: OriginDestinationFunction) => {
    getOriginDestination = func;
};
