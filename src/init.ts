import axios from 'axios';
import { HorseConfiguration } from '../definitions/HorseConfiguration';
import { add as addAxiosObject } from './AxiosObjectMap';
import { add as addDestination } from './Destinations';
import { set as setOriginDestinationFunction } from './getOriginDestinationFunction';

/**
 *
 * Initializes all configurations and axios objects for horse package to work properly.
 * Note: if some of your destinations require authentication,
 * make sure you do it before calling this function.
 * @param config Configuration object for destinations
 */
export const init = (config: HorseConfiguration) => {
    config.destinations.forEach(destination => {
        destination.enabled &&
            addAxiosObject(
                destination.name,
                axios.create({
                    baseURL: destination.baseUrl,
                    headers: destination.headers,
                    timeout: destination.timeout
                })
            );

        addDestination(destination);
    });

    config.getOriginDestination &&
        setOriginDestinationFunction(config.getOriginDestination);
};
