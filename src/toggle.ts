import axios from 'axios';
import { add as addAxiosObject, remove as removeAxiosObject } from './AxiosObjectMap';
import { getAll as getAllDestinations } from './Destinations';

/**
 *
 * Enables a destinations.
 * @param destinationName the name of the destination you want to enable
 */
export const enable = (destinationName: string) => {
    const destination = getAllDestinations().find(dest => dest.name === destinationName);
    addAxiosObject(
        destinationName,
        axios.create({
            baseURL: destination.baseUrl,
            headers: destination.headers,
            timeout: destination.timeout
        })
    );

    destination.enabled = true;
};

/**
 *
 * Disables a destinations.
 * @param destinationName the name of the destination you want to disable
 */
export const disable = (destinationName: string) => {
    const destination = getAllDestinations().find(dest => dest.name === destinationName);
    removeAxiosObject(destinationName);

    destination.enabled = false;
};
