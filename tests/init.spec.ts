const addAxiosObject = jest.fn();
jest.doMock('../src/AxiosObjectMap', () => ({ add: addAxiosObject }));

const addDestination = jest.fn();
jest.doMock('../src/Destinations', () => ({ add: addDestination }));

const setOriginDestinationFunction = jest.fn();
jest.doMock('../src/getOriginDestinationFunction', () => ({
    set: setOriginDestinationFunction
}));

import { init } from '../src/init';

describe('init', () => {
    describe('no destinations provided', () => {
        beforeAll(() => {
            jest.clearAllMocks();
            init({ destinations: [] });
        });

        it('should do nothing', () => {
            expect(addAxiosObject).not.toHaveBeenCalled();
            expect(addDestination).not.toHaveBeenCalled();
            expect(setOriginDestinationFunction).not.toHaveBeenCalled();
        });
    });

    describe('single destination', () => {
        const destTest = {
            name: 'flight-tower',
            baseUrl: 'https://flighttower.com',
            headers: {
                id: '123',
                token: '123456'
            },
            timeout: 10000,
            enabled: true,
            actions: [
                {
                    name: 'takeOff',
                    endpoint: 'takeoff',
                    shouldDeliver: () => true
                },
                {
                    name: 'land',
                    endpoint: 'land',
                    shouldDeliver: () => false
                }
            ]
        };

        beforeAll(() => {
            jest.clearAllMocks();
            init({
                destinations: [destTest]
            });
        });

        it('should init an axios object', () => {
            expect(addAxiosObject).toBeCalledTimes(1);
            expect(addAxiosObject).toBeCalledWith('flight-tower', expect.anything());
        });

        it('should have one destination stored', () => {
            expect(addDestination).toBeCalledTimes(1);
            expect(addDestination).toBeCalledWith(destTest);
        });

        it('should not define an origin destination function', () => {
            expect(setOriginDestinationFunction).not.toBeCalled();
        });
    });

    describe('multiple destinations', () => {
        const destsTest = [
            {
                name: 'flight-tower',
                baseUrl: 'https://flighttower.com',
                headers: {
                    id: '123',
                    token: '123456'
                },
                timeout: 10000,
                enabled: true,
                actions: [
                    {
                        name: 'takeOff',
                        endpoint: 'takeoff',
                        shouldDeliver: () => true
                    },
                    {
                        name: 'land',
                        endpoint: 'land',
                        shouldDeliver: () => false
                    }
                ]
            },
            {
                name: 'car',
                baseUrl: 'https://www.car.com',
                headers: {
                    'api-key': '123456'
                },
                timeout: 3000,
                enabled: false,
                actions: [
                    {
                        name: 'greenLight',
                        endpoint: 'lights/green',
                        shouldDeliver: () => true
                    },
                    {
                        name: 'redLight',
                        endpoint: 'lights/red',
                        shouldDeliver: () => true
                    },
                    {
                        name: 'yellowLight',
                        endpoint: 'lights/yellow',
                        shouldDeliver: () => false
                    }
                ]
            },
            {
                name: 'commander',
                baseUrl: 'https://www.commander.com',
                headers: {},
                timeout: 1000,
                enabled: true,
                actions: [
                    {
                        name: 'missionIsDone',
                        endpoint: 'mission.status.done',
                        shouldDeliver: () => true
                    },
                    {
                        name: 'readyForDuty',
                        endpoint: 'duty.status.ready',
                        shouldDeliver: () => true
                    }
                ]
            }
        ];

        beforeAll(() => {
            jest.clearAllMocks();
            init({ destinations: destsTest });
        });

        it('should init axios objects for enabled destinations', () => {
            expect(addAxiosObject).toBeCalledTimes(2);
            expect(addAxiosObject).toBeCalledWith('flight-tower', expect.anything());
            expect(addAxiosObject).toBeCalledWith('commander', expect.anything());
        });

        it('should not create axios object for disabled destination', () => {
            expect(addAxiosObject).not.toBeCalledWith('car', expect.anything());
        });

        it('should have all destinations stored', () => {
            expect(addDestination).toBeCalledTimes(3);
            expect(addDestination).toBeCalledWith(destsTest[0]);
            expect(addDestination).toBeCalledWith(destsTest[1]);
            expect(addDestination).toBeCalledWith(destsTest[2]);
        });

        it('should not define an origin destination function', () => {
            expect(setOriginDestinationFunction).not.toBeCalled();
        });
    });
});
